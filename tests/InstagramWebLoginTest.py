__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "11.01.2024 23:59"

from unittest import IsolatedAsyncioTestCase

import aiohttp

from instagram_auth.common.model.InstagramShortUser import InstagramShortUser
from instagram_auth.service.WebLoginService import WebLoginService
from tests.TestPropertiesService import TestPropertiesService


class InstagramWebLoginTest(IsolatedAsyncioTestCase):

    def setUp(self) -> None:
        self.web_service = WebLoginService()
        properties: dict = TestPropertiesService().load()
        print(f"{properties=}")
        self.credentials = {
            "username": properties.get("credentials", {}).get("login"),
            "password": properties.get("credentials", {}).get("password")
        }
        self.sessionid: str = properties.get("session_id")
        self.login_nonce: str = properties.get("login_nonce")
        self.user_id: str = self.sessionid.split(":")[0]

    async def test_web_login(self):
        """
        required: valid username and password
        """
        user, session_id = await self.web_service.login(**self.credentials)
        print(f"{user=}, {session_id=}")
        self.assertIsInstance(user, InstagramShortUser, "Should be InstagramShortUser")
        self.assertIsInstance(session_id, str, "Should be string")

    async def test_csrf_token(self):
        """
        required: none
        """
        session = aiohttp.ClientSession()
        csrf_token = await self.web_service.csrf_token(session)
        print(f"{csrf_token=}")
        await session.close()
        self.assertIsInstance(csrf_token, str, "Should be string")

    async def test_login_nonce(self):
        """
        required: valid sessionid
        """
        session = aiohttp.ClientSession()
        login_nonce = await self.web_service.login_nonce(session, self.sessionid)
        print(f'{login_nonce=}')
        await session.close()
        self.assertIsInstance(login_nonce, str, "Should be string")

    async def test_issue_new_sessionid(self):
        """
        required: valid sessionid and user_id
        """
        login_nonce, sessionid = await self.web_service.reissue_session_id(self.sessionid, self.user_id)
        print(f"{login_nonce=}, {sessionid=}")
        self.assertIsInstance(login_nonce, str, "Should be string")
        self.assertIsInstance(sessionid, str, "Should be string")

    async def test_issue_new_sessionid_with_login_nonce(self):
        """
        required: new valid sessionid, valid user_id and login_nonce
        """
        login_nonce, sessionid = await self.web_service.reissue_session_id(self.sessionid, self.user_id, self.login_nonce)
        print(f"{login_nonce=}, {sessionid=}")
        self.assertIsInstance(login_nonce, str, "Should be string")
        self.assertIsInstance(sessionid, str, "Should be string")
